[
    {
        "customer": {
            "requestRef": {
                "Id": "a4O1C000000a3AiUAI",
                "customerName": "Generic-1",
                "qbStatus": null,
                "qbEditSequence": null,
                "qbListId": null,
                "lastModifiedDate": "2018-06-04T18:17:00.000Z"
            },
            "account": {
                "Id": "0011C00001wFs77QAC",
                "name": "Generic",
                "billingCity": "not listed",
                "billingStreet": "not listed",
                "billingStateProvinceId": "a0o1500000ABoKcAAL",
                "billingStateProvinceName": "Wisconsin",
                "billingZipPostalCode": "54956",
                "msp": "Qualivis",
                "requestRef": {
                    "Id": "0011C00001wFs77QAC",
                    "qbStatus": null,
                    "qbEditSequence": null,
                    "qbListId": null
                }
            },
            "division": {
                "Id": "a4F1C0000008lT1UAI",
                "name": "Travel Nursing",
                "number": "1"
            },
            "billingCard": {
                "Id": "a4N1C000000LCsYUAW",
                "Billing_Contact__c": "",
                "Billing_Contact_Phone__c": "",
                "Payment_Terms__c": "Net 30",
                "Billing_State__c": "",
                "Billing_City__c": "",
                "Billing_Frequency__c": "Weekly",
                "type": "Billing_Card__c",
                "Billing_Address__c": "",
                "Invoice_Email_Address__c": "",
                "name": "BC-111",
                "Billing_Address_2__c": "",
                "Division__c": "a4F1C0000008lT1UAI",
                "Billing_Card_Name_text__c": "Generic Test",
                "Billing_Zip__c": "37934"
            }
        },
        "invoice": {
            "requestRef": {
                "Id": "a2oc000000MHtVeAAL",
                "name": "200067",
                "qbStatus": null,
                "lastModifiedDate": "2018-12-19T15:15:41.000Z"
            },
            "Billing_Contact__c": null,
            "Billing_Contact_Phone__c": null,
            "Date__c": "2018-12-19T15:15:41.000Z",
            "LastReferencedDate": "2018-11-03",
            "Billing_City__c": null,
            "type": "Invoice__c",
            "CreatedById": "005150000066ZqFAAU",
            "OwnerId": "005150000066ZqFAAU",
            "Billing_Card__c": "a4N1C000000LCsYUAW",
            "Payment_Terms__c": "Net 30",
            "IsDeleted": "false",
            "VMS_Fee__c": null,
            "Billing_State__c": null,
            "LastViewedDate": "2019-01-04T22:09:22.000Z",
            "Billing_Address__c": null,
            "SystemModstamp": "2018-12-19T15:15:41.000Z",
            "Discounted_Total_Bill__c": "5360.0",
            "Billing_Address_2__c": null,
            "CreatedDate": "2018-12-19T15:15:40.000Z",
            "Division__c": "a4F1C0000008lT1UAI",
            "LastActivityDate": null,
            "LastModifiedById": "005150000066ZqFAAU",
            "VMS_Discount__c": "0.0",
            "Billing_Zip__c": null
        }
    },
    {
        "customer": {
            "requestRef": {
                "Id": "a4O1C000000a3cXUAQ",
                "customerName": "Kaiser Permanente West Los Angeles Me-1",
                "qbStatus": null,
                "qbEditSequence": null,
                "qbListId": null,
                "lastModifiedDate": "2018-06-14T22:11:49.000Z"
            },
            "account": {
                "Id": "0011500001Lqg2aAAB",
                "name": "Kaiser Permanente West Los Angeles Medical Center",
                "billingCity": "Los Angeles",
                "billingStreet": "6041 Cadillac Ave",
                "billingStateProvinceId": "a0o1500000ABoJuAAL",
                "billingStateProvinceName": "California",
                "billingZipPostalCode": "90034",
                "msp": "AMN Healthcare",
                "requestRef": {
                    "Id": "0011500001Lqg2aAAB",
                    "qbStatus": null,
                    "qbEditSequence": null,
                    "qbListId": null
                }
            },
            "division": {
                "Id": "a4F1C0000008lT1UAI",
                "name": "Travel Nursing",
                "number": "1"
            },
            "billingCard": {
                "Id": "a4N1C000000LCoHUAW",
                "Billing_Contact__c": "",
                "Billing_Contact_Phone__c": "",
                "Payment_Terms__c": "",
                "Billing_State__c": "",
                "Billing_City__c": "",
                "Billing_Frequency__c": "Weekly",
                "type": "Billing_Card__c",
                "Billing_Address__c": "",
                "Invoice_Email_Address__c": "",
                "name": "BC-69",
                "Billing_Address_2__c": "",
                "Division__c": "a4F1C0000008lT1UAI",
                "Billing_Card_Name_text__c": "AMN - Kaiser",
                "Billing_Zip__c": ""
            }
        },
        "invoice": {
            "requestRef": {
                "Id": "a2oc000000MHtVgAAL",
                "name": "200069",
                "qbStatus": null,
                "lastModifiedDate": "2018-12-19T15:15:41.000Z"
            },
            "Billing_Contact__c": null,
            "Billing_Contact_Phone__c": null,
            "Date__c": "2018-12-19T15:15:41.000Z",
            "LastReferencedDate": "2018-11-17",
            "Billing_City__c": null,
            "type": "Invoice__c",
            "CreatedById": "005150000066ZqFAAU",
            "OwnerId": "005150000066ZqFAAU",
            "Billing_Card__c": "a4N1C000000LCoHUAW",
            "Payment_Terms__c": null,
            "IsDeleted": "false",
            "VMS_Fee__c": null,
            "Billing_State__c": null,
            "LastViewedDate": null,
            "Billing_Address__c": null,
            "SystemModstamp": "2018-12-19T15:15:41.000Z",
            "Discounted_Total_Bill__c": "600.0",
            "Billing_Address_2__c": null,
            "CreatedDate": "2018-12-19T15:15:41.000Z",
            "Division__c": "a4F1C0000008lT1UAI",
            "LastActivityDate": null,
            "LastModifiedById": "005150000066ZqFAAU",
            "VMS_Discount__c": "0.0",
            "Billing_Zip__c": null
        }
    },
    {
        "customer": {
            "requestRef": {
                "Id": "a4O1C000000a3cXUAQ",
                "customerName": "Kaiser Permanente West Los Angeles Me-1",
                "qbStatus": null,
                "qbEditSequence": null,
                "qbListId": null,
                "lastModifiedDate": "2018-06-14T22:11:49.000Z"
            },
            "account": {
                "Id": "0011500001Lqg2aAAB",
                "name": "Kaiser Permanente West Los Angeles Medical Center",
                "billingCity": "Los Angeles",
                "billingStreet": "6041 Cadillac Ave",
                "billingStateProvinceId": "a0o1500000ABoJuAAL",
                "billingStateProvinceName": "California",
                "billingZipPostalCode": "90034",
                "msp": "AMN Healthcare",
                "requestRef": {
                    "Id": "0011500001Lqg2aAAB",
                    "qbStatus": null,
                    "qbEditSequence": null,
                    "qbListId": null
                }
            },
            "division": {
                "Id": "a4F1C0000008lT1UAI",
                "name": "Travel Nursing",
                "number": "1"
            },
            "billingCard": {
                "Id": "a4N1C000000LCoHUAW",
                "Billing_Contact__c": "",
                "Billing_Contact_Phone__c": "",
                "Payment_Terms__c": "",
                "Billing_State__c": "",
                "Billing_City__c": "",
                "Billing_Frequency__c": "Weekly",
                "type": "Billing_Card__c",
                "Billing_Address__c": "",
                "Invoice_Email_Address__c": "",
                "name": "BC-69",
                "Billing_Address_2__c": "",
                "Division__c": "a4F1C0000008lT1UAI",
                "Billing_Card_Name_text__c": "AMN - Kaiser",
                "Billing_Zip__c": ""
            }
        },
        "invoice": {
            "requestRef": {
                "Id": "a2oc000000MHtVhAAL",
                "name": "200070",
                "qbStatus": null,
                "lastModifiedDate": "2018-12-19T15:15:41.000Z"
            },
            "Billing_Contact__c": null,
            "Billing_Contact_Phone__c": null,
            "Date__c": "2018-12-19T15:15:41.000Z",
            "LastReferencedDate": "2019-01-12",
            "Billing_City__c": null,
            "type": "Invoice__c",
            "CreatedById": "005150000066ZqFAAU",
            "OwnerId": "005150000066ZqFAAU",
            "Billing_Card__c": "a4N1C000000LCoHUAW",
            "Payment_Terms__c": null,
            "IsDeleted": "false",
            "VMS_Fee__c": null,
            "Billing_State__c": null,
            "LastViewedDate": null,
            "Billing_Address__c": null,
            "SystemModstamp": "2018-12-19T15:15:41.000Z",
            "Discounted_Total_Bill__c": "1200.0",
            "Billing_Address_2__c": null,
            "CreatedDate": "2018-12-19T15:15:41.000Z",
            "Division__c": "a4F1C0000008lT1UAI",
            "LastActivityDate": null,
            "LastModifiedById": "005150000066ZqFAAU",
            "VMS_Discount__c": "0.0",
            "Billing_Zip__c": null
        }
    },
    {
        "customer": {
            "requestRef": {
                "Id": "a4O1C000000a3cXUAQ",
                "customerName": "Kaiser Permanente West Los Angeles Me-1",
                "qbStatus": null,
                "qbEditSequence": null,
                "qbListId": null,
                "lastModifiedDate": "2018-06-14T22:11:49.000Z"
            },
            "account": {
                "Id": "0011500001Lqg2aAAB",
                "name": "Kaiser Permanente West Los Angeles Medical Center",
                "billingCity": "Los Angeles",
                "billingStreet": "6041 Cadillac Ave",
                "billingStateProvinceId": "a0o1500000ABoJuAAL",
                "billingStateProvinceName": "California",
                "billingZipPostalCode": "90034",
                "msp": "AMN Healthcare",
                "requestRef": {
                    "Id": "0011500001Lqg2aAAB",
                    "qbStatus": null,
                    "qbEditSequence": null,
                    "qbListId": null
                }
            },
            "division": {
                "Id": "a4F1C0000008lT1UAI",
                "name": "Travel Nursing",
                "number": "1"
            },
            "billingCard": {
                "Id": "a4N1C000000LCoHUAW",
                "Billing_Contact__c": "",
                "Billing_Contact_Phone__c": "",
                "Payment_Terms__c": "",
                "Billing_State__c": "",
                "Billing_City__c": "",
                "Billing_Frequency__c": "Weekly",
                "type": "Billing_Card__c",
                "Billing_Address__c": "",
                "Invoice_Email_Address__c": "",
                "name": "BC-69",
                "Billing_Address_2__c": "",
                "Division__c": "a4F1C0000008lT1UAI",
                "Billing_Card_Name_text__c": "AMN - Kaiser",
                "Billing_Zip__c": ""
            }
        },
        "invoice": {
            "requestRef": {
                "Id": "a2oc000000MHtViAAL",
                "name": "200071",
                "qbStatus": null,
                "lastModifiedDate": "2018-12-19T15:15:42.000Z"
            },
            "Billing_Contact__c": null,
            "Billing_Contact_Phone__c": null,
            "Date__c": "2018-12-19T15:15:42.000Z",
            "LastReferencedDate": "2019-01-26",
            "Billing_City__c": null,
            "type": "Invoice__c",
            "CreatedById": "005150000066ZqFAAU",
            "OwnerId": "005150000066ZqFAAU",
            "Billing_Card__c": "a4N1C000000LCoHUAW",
            "Payment_Terms__c": null,
            "IsDeleted": "false",
            "VMS_Fee__c": null,
            "Billing_State__c": null,
            "LastViewedDate": null,
            "Billing_Address__c": null,
            "SystemModstamp": "2018-12-19T15:15:42.000Z",
            "Discounted_Total_Bill__c": "600.0",
            "Billing_Address_2__c": null,
            "CreatedDate": "2018-12-19T15:15:41.000Z",
            "Division__c": "a4F1C0000008lT1UAI",
            "LastActivityDate": null,
            "LastModifiedById": "005150000066ZqFAAU",
            "VMS_Discount__c": "0.0",
            "Billing_Zip__c": null
        }
    },
    {
        "customer": {
            "requestRef": {
                "Id": "a4O1C000000a3cXUAQ",
                "customerName": "Kaiser Permanente West Los Angeles Me-1",
                "qbStatus": null,
                "qbEditSequence": null,
                "qbListId": null,
                "lastModifiedDate": "2018-06-14T22:11:49.000Z"
            },
            "account": {
                "Id": "0011500001Lqg2aAAB",
                "name": "Kaiser Permanente West Los Angeles Medical Center",
                "billingCity": "Los Angeles",
                "billingStreet": "6041 Cadillac Ave",
                "billingStateProvinceId": "a0o1500000ABoJuAAL",
                "billingStateProvinceName": "California",
                "billingZipPostalCode": "90034",
                "msp": "AMN Healthcare",
                "requestRef": {
                    "Id": "0011500001Lqg2aAAB",
                    "qbStatus": null,
                    "qbEditSequence": null,
                    "qbListId": null
                }
            },
            "division": {
                "Id": "a4F1C0000008lT1UAI",
                "name": "Travel Nursing",
                "number": "1"
            },
            "billingCard": {
                "Id": "a4N1C000000LCoHUAW",
                "Billing_Contact__c": "",
                "Billing_Contact_Phone__c": "",
                "Payment_Terms__c": "",
                "Billing_State__c": "",
                "Billing_City__c": "",
                "Billing_Frequency__c": "Weekly",
                "type": "Billing_Card__c",
                "Billing_Address__c": "",
                "Invoice_Email_Address__c": "",
                "name": "BC-69",
                "Billing_Address_2__c": "",
                "Division__c": "a4F1C0000008lT1UAI",
                "Billing_Card_Name_text__c": "AMN - Kaiser",
                "Billing_Zip__c": ""
            }
        },
        "invoice": {
            "requestRef": {
                "Id": "a2oc000000MHtVjAAL",
                "name": "200072",
                "qbStatus": null,
                "lastModifiedDate": "2018-12-19T15:15:42.000Z"
            },
            "Billing_Contact__c": null,
            "Billing_Contact_Phone__c": null,
            "Date__c": "2018-12-19T15:15:42.000Z",
            "LastReferencedDate": "2019-02-02",
            "Billing_City__c": null,
            "type": "Invoice__c",
            "CreatedById": "005150000066ZqFAAU",
            "OwnerId": "005150000066ZqFAAU",
            "Billing_Card__c": "a4N1C000000LCoHUAW",
            "Payment_Terms__c": null,
            "IsDeleted": "false",
            "VMS_Fee__c": null,
            "Billing_State__c": null,
            "LastViewedDate": null,
            "Billing_Address__c": null,
            "SystemModstamp": "2018-12-19T15:15:42.000Z",
            "Discounted_Total_Bill__c": "1200.0",
            "Billing_Address_2__c": null,
            "CreatedDate": "2018-12-19T15:15:42.000Z",
            "Division__c": "a4F1C0000008lT1UAI",
            "LastActivityDate": null,
            "LastModifiedById": "005150000066ZqFAAU",
            "VMS_Discount__c": "0.0",
            "Billing_Zip__c": null
        }
    },
    {
        "customer": {
            "requestRef": {
                "Id": "a4O1C000000a3cXUAQ",
                "customerName": "Kaiser Permanente West Los Angeles Me-1",
                "qbStatus": null,
                "qbEditSequence": null,
                "qbListId": null,
                "lastModifiedDate": "2018-06-14T22:11:49.000Z"
            },
            "account": {
                "Id": "0011500001Lqg2aAAB",
                "name": "Kaiser Permanente West Los Angeles Medical Center",
                "billingCity": "Los Angeles",
                "billingStreet": "6041 Cadillac Ave",
                "billingStateProvinceId": "a0o1500000ABoJuAAL",
                "billingStateProvinceName": "California",
                "billingZipPostalCode": "90034",
                "msp": "AMN Healthcare",
                "requestRef": {
                    "Id": "0011500001Lqg2aAAB",
                    "qbStatus": null,
                    "qbEditSequence": null,
                    "qbListId": null
                }
            },
            "division": {
                "Id": "a4F1C0000008lT1UAI",
                "name": "Travel Nursing",
                "number": "1"
            },
            "billingCard": {
                "Id": "a4N1C000000LCoHUAW",
                "Billing_Contact__c": "",
                "Billing_Contact_Phone__c": "",
                "Payment_Terms__c": "",
                "Billing_State__c": "",
                "Billing_City__c": "",
                "Billing_Frequency__c": "Weekly",
                "type": "Billing_Card__c",
                "Billing_Address__c": "",
                "Invoice_Email_Address__c": "",
                "name": "BC-69",
                "Billing_Address_2__c": "",
                "Division__c": "a4F1C0000008lT1UAI",
                "Billing_Card_Name_text__c": "AMN - Kaiser",
                "Billing_Zip__c": ""
            }
        },
        "invoice": {
            "requestRef": {
                "Id": "a2oc000000MHtVlAAL",
                "name": "200074",
                "qbStatus": null,
                "lastModifiedDate": "2018-12-19T15:15:42.000Z"
            },
            "Billing_Contact__c": null,
            "Billing_Contact_Phone__c": null,
            "Date__c": "2018-12-19T15:15:42.000Z",
            "LastReferencedDate": "2019-08-03",
            "Billing_City__c": null,
            "type": "Invoice__c",
            "CreatedById": "005150000066ZqFAAU",
            "OwnerId": "005150000066ZqFAAU",
            "Billing_Card__c": "a4N1C000000LCoHUAW",
            "Payment_Terms__c": null,
            "IsDeleted": "false",
            "VMS_Fee__c": null,
            "Billing_State__c": null,
            "LastViewedDate": null,
            "Billing_Address__c": null,
            "SystemModstamp": "2018-12-19T15:15:42.000Z",
            "Discounted_Total_Bill__c": "600.0",
            "Billing_Address_2__c": null,
            "CreatedDate": "2018-12-19T15:15:42.000Z",
            "Division__c": "a4F1C0000008lT1UAI",
            "LastActivityDate": null,
            "LastModifiedById": "005150000066ZqFAAU",
            "VMS_Discount__c": "0.0",
            "Billing_Zip__c": null
        }
    },
    {
        "customer": {
            "requestRef": {
                "Id": "a4O1C000000a3JzUAI",
                "customerName": "Mission Hospital Regional Medical Cen-1",
                "qbStatus": null,
                "qbEditSequence": null,
                "qbListId": null,
                "lastModifiedDate": "2018-06-11T15:41:50.000Z"
            },
            "account": {
                "Id": "0011500001LqgVDAAZ",
                "name": "Mission Hospital Regional Medical Center",
                "billingCity": "Mission Viejo",
                "billingStreet": "27700 Medical Center Road",
                "billingStateProvinceId": "a0o1500000ABoJuAAL",
                "billingStateProvinceName": "California",
                "billingZipPostalCode": "92691",
                "msp": "AMN Healthcare",
                "requestRef": {
                    "Id": "0011500001LqgVDAAZ",
                    "qbStatus": null,
                    "qbEditSequence": null,
                    "qbListId": null
                }
            },
            "division": {
                "Id": "a4F1C0000008lT1UAI",
                "name": "Travel Nursing",
                "number": "1"
            },
            "billingCard": {
                "Id": "a4N1C000000LCxnUAG",
                "Billing_Contact__c": "",
                "Billing_Contact_Phone__c": "",
                "Payment_Terms__c": "Net 45",
                "Billing_State__c": "",
                "Billing_City__c": "",
                "Billing_Frequency__c": "Weekly",
                "type": "Billing_Card__c",
                "Billing_Address__c": "",
                "Invoice_Email_Address__c": "",
                "name": "BC-168",
                "Billing_Address_2__c": "",
                "Division__c": "a4F1C0000008lT1UAI",
                "Billing_Card_Name_text__c": "AMN - Mission Hospital Regional Medical Center",
                "Billing_Zip__c": ""
            }
        },
        "invoice": {
            "requestRef": {
                "Id": "a2oc000000MHtVNAA1",
                "name": "200050",
                "qbStatus": null,
                "lastModifiedDate": "2018-12-19T15:15:36.000Z"
            },
            "Billing_Contact__c": null,
            "Billing_Contact_Phone__c": null,
            "Date__c": "2018-12-19T15:15:36.000Z",
            "LastReferencedDate": "2018-04-28",
            "Billing_City__c": null,
            "type": "Invoice__c",
            "CreatedById": "005150000066ZqFAAU",
            "OwnerId": "005150000066ZqFAAU",
            "Billing_Card__c": "a4N1C000000LCxnUAG",
            "Payment_Terms__c": "Net 45",
            "IsDeleted": "false",
            "VMS_Fee__c": "5.0",
            "Billing_State__c": null,
            "LastViewedDate": null,
            "Billing_Address__c": null,
            "SystemModstamp": "2018-12-19T15:15:36.000Z",
            "Discounted_Total_Bill__c": "0.0",
            "Billing_Address_2__c": null,
            "CreatedDate": "2018-12-19T15:15:36.000Z",
            "Division__c": "a4F1C0000008lT1UAI",
            "LastActivityDate": null,
            "LastModifiedById": "005150000066ZqFAAU",
            "VMS_Discount__c": "0.0",
            "Billing_Zip__c": null
        }
    },
    {
        "customer": {
            "requestRef": {
                "Id": "a4O1C000000a3PiUAI",
                "customerName": "Palmetto Health Richland-2",
                "qbStatus": null,
                "qbEditSequence": null,
                "qbListId": null,
                "lastModifiedDate": "2018-06-11T20:25:08.000Z"
            },
            "account": {
                "Id": "0011500001gC4OHAA0",
                "name": "Palmetto Health Richland",
                "billingCity": "Columbia",
                "billingStreet": "5 Richland Medical Park",
                "billingStateProvinceId": "a0o1500000ABoKTAA1",
                "billingStateProvinceName": "South Carolina",
                "billingZipPostalCode": "29203",
                "msp": "Healthcare-StaffingServices",
                "requestRef": {
                    "Id": "0011500001gC4OHAA0",
                    "qbStatus": null,
                    "qbEditSequence": null,
                    "qbListId": null
                }
            },
            "division": {
                "Id": "a4F1C0000008lT2UAI",
                "name": "Allied Health Tech",
                "number": "2"
            },
            "billingCard": {
                "Id": "a4N1C000000LD2YUAW",
                "Billing_Contact__c": "",
                "Billing_Contact_Phone__c": "",
                "Payment_Terms__c": "",
                "Billing_State__c": "",
                "Billing_City__c": "",
                "Billing_Frequency__c": "Weekly",
                "type": "Billing_Card__c",
                "Billing_Address__c": "",
                "Invoice_Email_Address__c": "",
                "name": "BC-227",
                "Billing_Address_2__c": "",
                "Division__c": "a4F1C0000008lT2UAI",
                "Billing_Card_Name_text__c": "Qualivis - Palmetto Health System",
                "Billing_Zip__c": ""
            }
        },
        "invoice": {
            "requestRef": {
                "Id": "a2oc000000MHtVMAA1",
                "name": "200049",
                "qbStatus": null,
                "lastModifiedDate": "2018-12-19T15:15:36.000Z"
            },
            "Billing_Contact__c": null,
            "Billing_Contact_Phone__c": null,
            "Date__c": "2018-12-19T15:15:36.000Z",
            "LastReferencedDate": "2018-04-28",
            "Billing_City__c": null,
            "type": "Invoice__c",
            "CreatedById": "005150000066ZqFAAU",
            "OwnerId": "005150000066ZqFAAU",
            "Billing_Card__c": "a4N1C000000LD2YUAW",
            "Payment_Terms__c": null,
            "IsDeleted": "false",
            "VMS_Fee__c": null,
            "Billing_State__c": null,
            "LastViewedDate": null,
            "Billing_Address__c": null,
            "SystemModstamp": "2018-12-19T15:15:36.000Z",
            "Discounted_Total_Bill__c": "0.0",
            "Billing_Address_2__c": null,
            "CreatedDate": "2018-12-19T15:15:36.000Z",
            "Division__c": "a4F1C0000008lT2UAI",
            "LastActivityDate": null,
            "LastModifiedById": "005150000066ZqFAAU",
            "VMS_Discount__c": "0.0",
            "Billing_Zip__c": null
        }
    },
    {
        "customer": {
            "requestRef": {
                "Id": "a4O1C000000a3PiUAI",
                "customerName": "Palmetto Health Richland-2",
                "qbStatus": null,
                "qbEditSequence": null,
                "qbListId": null,
                "lastModifiedDate": "2018-06-11T20:25:08.000Z"
            },
            "account": {
                "Id": "0011500001gC4OHAA0",
                "name": "Palmetto Health Richland",
                "billingCity": "Columbia",
                "billingStreet": "5 Richland Medical Park",
                "billingStateProvinceId": "a0o1500000ABoKTAA1",
                "billingStateProvinceName": "South Carolina",
                "billingZipPostalCode": "29203",
                "msp": "Healthcare-StaffingServices",
                "requestRef": {
                    "Id": "0011500001gC4OHAA0",
                    "qbStatus": null,
                    "qbEditSequence": null,
                    "qbListId": null
                }
            },
            "division": {
                "Id": "a4F1C0000008lT2UAI",
                "name": "Allied Health Tech",
                "number": "2"
            },
            "billingCard": {
                "Id": "a4N1C000000LD2YUAW",
                "Billing_Contact__c": "",
                "Billing_Contact_Phone__c": "",
                "Payment_Terms__c": "",
                "Billing_State__c": "",
                "Billing_City__c": "",
                "Billing_Frequency__c": "Weekly",
                "type": "Billing_Card__c",
                "Billing_Address__c": "",
                "Invoice_Email_Address__c": "",
                "name": "BC-227",
                "Billing_Address_2__c": "",
                "Division__c": "a4F1C0000008lT2UAI",
                "Billing_Card_Name_text__c": "Qualivis - Palmetto Health System",
                "Billing_Zip__c": ""
            }
        },
        "invoice": {
            "requestRef": {
                "Id": "a2oc000000MHtVOAA1",
                "name": "200051",
                "qbStatus": null,
                "lastModifiedDate": "2018-12-19T15:15:37.000Z"
            },
            "Billing_Contact__c": null,
            "Billing_Contact_Phone__c": null,
            "Date__c": "2018-12-19T15:15:37.000Z",
            "LastReferencedDate": "2018-05-05",
            "Billing_City__c": null,
            "type": "Invoice__c",
            "CreatedById": "005150000066ZqFAAU",
            "OwnerId": "005150000066ZqFAAU",
            "Billing_Card__c": "a4N1C000000LD2YUAW",
            "Payment_Terms__c": null,
            "IsDeleted": "false",
            "VMS_Fee__c": null,
            "Billing_State__c": null,
            "LastViewedDate": null,
            "Billing_Address__c": null,
            "SystemModstamp": "2018-12-19T15:15:37.000Z",
            "Discounted_Total_Bill__c": "0.0",
            "Billing_Address_2__c": null,
            "CreatedDate": "2018-12-19T15:15:36.000Z",
            "Division__c": "a4F1C0000008lT2UAI",
            "LastActivityDate": null,
            "LastModifiedById": "005150000066ZqFAAU",
            "VMS_Discount__c": "0.0",
            "Billing_Zip__c": null
        }
    },
    {
        "customer": {
            "requestRef": {
                "Id": "a4O1C000000a3PiUAI",
                "customerName": "Palmetto Health Richland-2",
                "qbStatus": null,
                "qbEditSequence": null,
                "qbListId": null,
                "lastModifiedDate": "2018-06-11T20:25:08.000Z"
            },
            "account": {
                "Id": "0011500001gC4OHAA0",
                "name": "Palmetto Health Richland",
                "billingCity": "Columbia",
                "billingStreet": "5 Richland Medical Park",
                "billingStateProvinceId": "a0o1500000ABoKTAA1",
                "billingStateProvinceName": "South Carolina",
                "billingZipPostalCode": "29203",
                "msp": "Healthcare-StaffingServices",
                "requestRef": {
                    "Id": "0011500001gC4OHAA0",
                    "qbStatus": null,
                    "qbEditSequence": null,
                    "qbListId": null
                }
            },
            "division": {
                "Id": "a4F1C0000008lT2UAI",
                "name": "Allied Health Tech",
                "number": "2"
            },
            "billingCard": {
                "Id": "a4N1C000000LD2YUAW",
                "Billing_Contact__c": "",
                "Billing_Contact_Phone__c": "",
                "Payment_Terms__c": "",
                "Billing_State__c": "",
                "Billing_City__c": "",
                "Billing_Frequency__c": "Weekly",
                "type": "Billing_Card__c",
                "Billing_Address__c": "",
                "Invoice_Email_Address__c": "",
                "name": "BC-227",
                "Billing_Address_2__c": "",
                "Division__c": "a4F1C0000008lT2UAI",
                "Billing_Card_Name_text__c": "Qualivis - Palmetto Health System",
                "Billing_Zip__c": ""
            }
        },
        "invoice": {
            "requestRef": {
                "Id": "a2oc000000MHtVSAA1",
                "name": "200055",
                "qbStatus": null,
                "lastModifiedDate": "2018-12-19T15:15:37.000Z"
            },
            "Billing_Contact__c": null,
            "Billing_Contact_Phone__c": null,
            "Date__c": "2018-12-19T15:15:37.000Z",
            "LastReferencedDate": "2018-05-12",
            "Billing_City__c": null,
            "type": "Invoice__c",
            "CreatedById": "005150000066ZqFAAU",
            "OwnerId": "005150000066ZqFAAU",
            "Billing_Card__c": "a4N1C000000LD2YUAW",
            "Payment_Terms__c": null,
            "IsDeleted": "false",
            "VMS_Fee__c": null,
            "Billing_State__c": null,
            "LastViewedDate": null,
            "Billing_Address__c": null,
            "SystemModstamp": "2018-12-19T15:15:37.000Z",
            "Discounted_Total_Bill__c": "0.0",
            "Billing_Address_2__c": null,
            "CreatedDate": "2018-12-19T15:15:37.000Z",
            "Division__c": "a4F1C0000008lT2UAI",
            "LastActivityDate": null,
            "LastModifiedById": "005150000066ZqFAAU",
            "VMS_Discount__c": "0.0",
            "Billing_Zip__c": null
        }
    },
    {
        "customer": {
            "requestRef": {
                "Id": "a4O1C000000a3ZTUAY",
                "customerName": "Pinnacle Senior Care Of Wisconsin-3",
                "qbStatus": null,
                "qbEditSequence": null,
                "qbListId": null,
                "lastModifiedDate": "2018-06-13T17:29:04.000Z"
            },
            "account": {
                "Id": "0011C00001w1Hr0QAE",
                "name": "Pinnacle Senior Care Of Wisconsin",
                "billingCity": "West Allis",
                "billingStreet": "2514 S 102nd St.",
                "billingStateProvinceId": "a0o1500000ABoKcAAL",
                "billingStateProvinceName": "Wisconsin",
                "billingZipPostalCode": "53227",
                "msp": "FocusOne Solutions",
                "requestRef": {
                    "Id": "0011C00001w1Hr0QAE",
                    "qbStatus": null,
                    "qbEditSequence": null,
                    "qbListId": null
                }
            },
            "division": {
                "Id": "a4F1C0000008lT3UAI",
                "name": "Allied Health Therapy",
                "number": "3"
            },
            "billingCard": {
                "Id": "a4N1C000000LD6GUAW",
                "Billing_Contact__c": "0031500001jqE2iAAE",
                "Billing_Contact_Phone__c": "800-856-6574 ext 2160",
                "Payment_Terms__c": "Net 45",
                "Billing_State__c": "",
                "Billing_City__c": "",
                "Billing_Frequency__c": "Weekly",
                "type": "Billing_Card__c",
                "Billing_Address__c": "",
                "Invoice_Email_Address__c": "jfarrell@focusonesolutions.com",
                "name": "BC-273",
                "Billing_Address_2__c": "",
                "Division__c": "a4F1C0000008lT3UAI",
                "Billing_Card_Name_text__c": "FocusOne - Pinnacle - PT",
                "Billing_Zip__c": ""
            }
        },
        "invoice": {
            "requestRef": {
                "Id": "a2oc000000MHtVLAA1",
                "name": "200048",
                "qbStatus": null,
                "lastModifiedDate": "2018-12-19T15:15:35.000Z"
            },
            "Billing_Contact__c": "0031500001jqE2iAAE",
            "Billing_Contact_Phone__c": "800-856-6574 ext 2160",
            "Date__c": "2018-12-19T15:15:35.000Z",
            "LastReferencedDate": "2018-04-21",
            "Billing_City__c": null,
            "type": "Invoice__c",
            "CreatedById": "005150000066ZqFAAU",
            "OwnerId": "005150000066ZqFAAU",
            "Billing_Card__c": "a4N1C000000LD6GUAW",
            "Payment_Terms__c": "Net 45",
            "IsDeleted": "false",
            "VMS_Fee__c": null,
            "Billing_State__c": null,
            "LastViewedDate": null,
            "Billing_Address__c": null,
            "SystemModstamp": "2018-12-19T15:15:35.000Z",
            "Discounted_Total_Bill__c": "0.0",
            "Billing_Address_2__c": null,
            "CreatedDate": "2018-12-19T15:15:35.000Z",
            "Division__c": "a4F1C0000008lT3UAI",
            "LastActivityDate": null,
            "LastModifiedById": "005150000066ZqFAAU",
            "VMS_Discount__c": "0.0",
            "Billing_Zip__c": null
        }
    },
    {
        "customer": {
            "requestRef": {
                "Id": "a4O1C000000a3ZTUAY",
                "customerName": "Pinnacle Senior Care Of Wisconsin-3",
                "qbStatus": null,
                "qbEditSequence": null,
                "qbListId": null,
                "lastModifiedDate": "2018-06-13T17:29:04.000Z"
            },
            "account": {
                "Id": "0011C00001w1Hr0QAE",
                "name": "Pinnacle Senior Care Of Wisconsin",
                "billingCity": "West Allis",
                "billingStreet": "2514 S 102nd St.",
                "billingStateProvinceId": "a0o1500000ABoKcAAL",
                "billingStateProvinceName": "Wisconsin",
                "billingZipPostalCode": "53227",
                "msp": "FocusOne Solutions",
                "requestRef": {
                    "Id": "0011C00001w1Hr0QAE",
                    "qbStatus": null,
                    "qbEditSequence": null,
                    "qbListId": null
                }
            },
            "division": {
                "Id": "a4F1C0000008lT3UAI",
                "name": "Allied Health Therapy",
                "number": "3"
            },
            "billingCard": {
                "Id": "a4N1C000000LD6GUAW",
                "Billing_Contact__c": "0031500001jqE2iAAE",
                "Billing_Contact_Phone__c": "800-856-6574 ext 2160",
                "Payment_Terms__c": "Net 45",
                "Billing_State__c": "",
                "Billing_City__c": "",
                "Billing_Frequency__c": "Weekly",
                "type": "Billing_Card__c",
                "Billing_Address__c": "",
                "Invoice_Email_Address__c": "jfarrell@focusonesolutions.com",
                "name": "BC-273",
                "Billing_Address_2__c": "",
                "Division__c": "a4F1C0000008lT3UAI",
                "Billing_Card_Name_text__c": "FocusOne - Pinnacle - PT",
                "Billing_Zip__c": ""
            }
        },
        "invoice": {
            "requestRef": {
                "Id": "a2oc000000MHtVPAA1",
                "name": "200052",
                "qbStatus": null,
                "lastModifiedDate": "2018-12-19T15:15:37.000Z"
            },
            "Billing_Contact__c": "0031500001jqE2iAAE",
            "Billing_Contact_Phone__c": "800-856-6574 ext 2160",
            "Date__c": "2018-12-19T15:15:37.000Z",
            "LastReferencedDate": "2018-05-05",
            "Billing_City__c": null,
            "type": "Invoice__c",
            "CreatedById": "005150000066ZqFAAU",
            "OwnerId": "005150000066ZqFAAU",
            "Billing_Card__c": "a4N1C000000LD6GUAW",
            "Payment_Terms__c": "Net 45",
            "IsDeleted": "false",
            "VMS_Fee__c": null,
            "Billing_State__c": null,
            "LastViewedDate": null,
            "Billing_Address__c": null,
            "SystemModstamp": "2018-12-19T15:15:37.000Z",
            "Discounted_Total_Bill__c": "0.0",
            "Billing_Address_2__c": null,
            "CreatedDate": "2018-12-19T15:15:37.000Z",
            "Division__c": "a4F1C0000008lT3UAI",
            "LastActivityDate": null,
            "LastModifiedById": "005150000066ZqFAAU",
            "VMS_Discount__c": "0.0",
            "Billing_Zip__c": null
        }
    },
    {
        "customer": {
            "requestRef": {
                "Id": "a4O1C000000a3ZTUAY",
                "customerName": "Pinnacle Senior Care Of Wisconsin-3",
                "qbStatus": null,
                "qbEditSequence": null,
                "qbListId": null,
                "lastModifiedDate": "2018-06-13T17:29:04.000Z"
            },
            "account": {
                "Id": "0011C00001w1Hr0QAE",
                "name": "Pinnacle Senior Care Of Wisconsin",
                "billingCity": "West Allis",
                "billingStreet": "2514 S 102nd St.",
                "billingStateProvinceId": "a0o1500000ABoKcAAL",
                "billingStateProvinceName": "Wisconsin",
                "billingZipPostalCode": "53227",
                "msp": "FocusOne Solutions",
                "requestRef": {
                    "Id": "0011C00001w1Hr0QAE",
                    "qbStatus": null,
                    "qbEditSequence": null,
                    "qbListId": null
                }
            },
            "division": {
                "Id": "a4F1C0000008lT3UAI",
                "name": "Allied Health Therapy",
                "number": "3"
            },
            "billingCard": {
                "Id": "a4N1C000000LD6GUAW",
                "Billing_Contact__c": "0031500001jqE2iAAE",
                "Billing_Contact_Phone__c": "800-856-6574 ext 2160",
                "Payment_Terms__c": "Net 45",
                "Billing_State__c": "",
                "Billing_City__c": "",
                "Billing_Frequency__c": "Weekly",
                "type": "Billing_Card__c",
                "Billing_Address__c": "",
                "Invoice_Email_Address__c": "jfarrell@focusonesolutions.com",
                "name": "BC-273",
                "Billing_Address_2__c": "",
                "Division__c": "a4F1C0000008lT3UAI",
                "Billing_Card_Name_text__c": "FocusOne - Pinnacle - PT",
                "Billing_Zip__c": ""
            }
        },
        "invoice": {
            "requestRef": {
                "Id": "a2oc000000MHtVYAA1",
                "name": "200061",
                "qbStatus": null,
                "lastModifiedDate": "2018-12-19T15:15:39.000Z"
            },
            "Billing_Contact__c": "0031500001jqE2iAAE",
            "Billing_Contact_Phone__c": "800-856-6574 ext 2160",
            "Date__c": "2018-12-19T15:15:39.000Z",
            "LastReferencedDate": "2018-08-11",
            "Billing_City__c": null,
            "type": "Invoice__c",
            "CreatedById": "005150000066ZqFAAU",
            "OwnerId": "005150000066ZqFAAU",
            "Billing_Card__c": "a4N1C000000LD6GUAW",
            "Payment_Terms__c": "Net 45",
            "IsDeleted": "false",
            "VMS_Fee__c": null,
            "Billing_State__c": null,
            "LastViewedDate": null,
            "Billing_Address__c": null,
            "SystemModstamp": "2018-12-19T15:15:39.000Z",
            "Discounted_Total_Bill__c": "0.0",
            "Billing_Address_2__c": null,
            "CreatedDate": "2018-12-19T15:15:39.000Z",
            "Division__c": "a4F1C0000008lT3UAI",
            "LastActivityDate": null,
            "LastModifiedById": "005150000066ZqFAAU",
            "VMS_Discount__c": "0.0",
            "Billing_Zip__c": null
        }
    },
    {
        "customer": {
            "requestRef": {
                "Id": "a4O1C000000a3ltUAA",
                "customerName": "Absolute Total Care-4",
                "qbStatus": null,
                "qbEditSequence": null,
                "qbListId": null,
                "lastModifiedDate": "2018-06-21T19:29:49.000Z"
            },
            "account": {
                "Id": "0011C00001uQ8ZdQAK",
                "name": "Absolute Total Care",
                "billingCity": "Columbia",
                "billingStreet": "1441 Main St",
                "billingStateProvinceId": "a0o1500000ABoKTAA1",
                "billingStateProvinceName": "South Carolina",
                "billingZipPostalCode": "29201",
                "msp": "",
                "requestRef": {
                    "Id": "0011C00001uQ8ZdQAK",
                    "qbStatus": null,
                    "qbEditSequence": null,
                    "qbListId": null
                }
            },
            "division": {
                "Id": "a4F1C0000008laTUAQ",
                "name": "MedFi",
                "number": "4"
            },
            "billingCard": {
                "Id": "a4N1C000000LD6fUAG",
                "Billing_Contact__c": "0031C00002JRW9XQAX",
                "Billing_Contact_Phone__c": "",
                "Payment_Terms__c": "Net 45",
                "Billing_State__c": "a0o1500000ABoKEAA1",
                "Billing_City__c": "St Louis",
                "Billing_Frequency__c": "Weekly",
                "type": "Billing_Card__c",
                "Billing_Address__c": "7700 Forsyth Blvd",
                "Invoice_Email_Address__c": "rspetz@coordinatedcarehealth.com",
                "name": "BC-278",
                "Billing_Address_2__c": "",
                "Division__c": "a4F1C0000008laTUAQ",
                "Billing_Card_Name_text__c": "Centene",
                "Billing_Zip__c": "63105"
            }
        },
        "invoice": {
            "requestRef": {
                "Id": "a2oc000000MHtVJAA1",
                "name": "200046",
                "qbStatus": null,
                "lastModifiedDate": "2018-12-19T15:15:35.000Z"
            },
            "Billing_Contact__c": "0031C00002JRW9XQAX",
            "Billing_Contact_Phone__c": null,
            "Date__c": "2018-12-19T15:15:35.000Z",
            "LastReferencedDate": "2018-04-14",
            "Billing_City__c": "St Louis",
            "type": "Invoice__c",
            "CreatedById": "005150000066ZqFAAU",
            "OwnerId": "005150000066ZqFAAU",
            "Billing_Card__c": "a4N1C000000LD6fUAG",
            "Payment_Terms__c": "Net 45",
            "IsDeleted": "false",
            "VMS_Fee__c": null,
            "Billing_State__c": "Missouri",
            "LastViewedDate": null,
            "Billing_Address__c": "7700 Forsyth Blvd",
            "SystemModstamp": "2018-12-19T15:15:35.000Z",
            "Discounted_Total_Bill__c": "0.0",
            "Billing_Address_2__c": null,
            "CreatedDate": "2018-12-19T15:15:35.000Z",
            "Division__c": "a4F1C0000008laTUAQ",
            "LastActivityDate": null,
            "LastModifiedById": "005150000066ZqFAAU",
            "VMS_Discount__c": "0.0",
            "Billing_Zip__c": "63105"
        }
    },
    {
        "customer": {
            "requestRef": {
                "Id": "a4O1C000000a3ltUAA",
                "customerName": "Absolute Total Care-4",
                "qbStatus": null,
                "qbEditSequence": null,
                "qbListId": null,
                "lastModifiedDate": "2018-06-21T19:29:49.000Z"
            },
            "account": {
                "Id": "0011C00001uQ8ZdQAK",
                "name": "Absolute Total Care",
                "billingCity": "Columbia",
                "billingStreet": "1441 Main St",
                "billingStateProvinceId": "a0o1500000ABoKTAA1",
                "billingStateProvinceName": "South Carolina",
                "billingZipPostalCode": "29201",
                "msp": "",
                "requestRef": {
                    "Id": "0011C00001uQ8ZdQAK",
                    "qbStatus": null,
                    "qbEditSequence": null,
                    "qbListId": null
                }
            },
            "division": {
                "Id": "a4F1C0000008laTUAQ",
                "name": "MedFi",
                "number": "4"
            },
            "billingCard": {
                "Id": "a4N1C000000LD6fUAG",
                "Billing_Contact__c": "0031C00002JRW9XQAX",
                "Billing_Contact_Phone__c": "",
                "Payment_Terms__c": "Net 45",
                "Billing_State__c": "a0o1500000ABoKEAA1",
                "Billing_City__c": "St Louis",
                "Billing_Frequency__c": "Weekly",
                "type": "Billing_Card__c",
                "Billing_Address__c": "7700 Forsyth Blvd",
                "Invoice_Email_Address__c": "rspetz@coordinatedcarehealth.com",
                "name": "BC-278",
                "Billing_Address_2__c": "",
                "Division__c": "a4F1C0000008laTUAQ",
                "Billing_Card_Name_text__c": "Centene",
                "Billing_Zip__c": "63105"
            }
        },
        "invoice": {
            "requestRef": {
                "Id": "a2oc000000MHtVkAAL",
                "name": "200073",
                "qbStatus": null,
                "lastModifiedDate": "2018-12-19T15:15:42.000Z"
            },
            "Billing_Contact__c": "0031C00002JRW9XQAX",
            "Billing_Contact_Phone__c": null,
            "Date__c": "2018-12-19T15:15:42.000Z",
            "LastReferencedDate": "2019-06-15",
            "Billing_City__c": "St Louis",
            "type": "Invoice__c",
            "CreatedById": "005150000066ZqFAAU",
            "OwnerId": "005150000066ZqFAAU",
            "Billing_Card__c": "a4N1C000000LD6fUAG",
            "Payment_Terms__c": "Net 45",
            "IsDeleted": "false",
            "VMS_Fee__c": null,
            "Billing_State__c": "Missouri",
            "LastViewedDate": null,
            "Billing_Address__c": "7700 Forsyth Blvd",
            "SystemModstamp": "2018-12-19T15:15:42.000Z",
            "Discounted_Total_Bill__c": "1845.16",
            "Billing_Address_2__c": null,
            "CreatedDate": "2018-12-19T15:15:42.000Z",
            "Division__c": "a4F1C0000008laTUAQ",
            "LastActivityDate": null,
            "LastModifiedById": "005150000066ZqFAAU",
            "VMS_Discount__c": "0.0",
            "Billing_Zip__c": "63105"
        }
    },
    {
        "customer": {
            "requestRef": {
                "Id": "a4O1C000000a3gZUAQ",
                "customerName": "Kaiser Permanente Santa Clara Medical-1",
                "qbStatus": null,
                "qbEditSequence": null,
                "qbListId": null,
                "lastModifiedDate": "2018-06-16T17:21:40.000Z"
            },
            "account": {
                "Id": "0011500001Lqg2UAAR",
                "name": "Kaiser Permanente Santa Clara Medical Center",
                "billingCity": "Santa Clara",
                "billingStreet": "700 Lawrence Expressway",
                "billingStateProvinceId": "a0o1500000ABoJuAAL",
                "billingStateProvinceName": "California",
                "billingZipPostalCode": "95051",
                "msp": "AMN Healthcare",
                "requestRef": {
                    "Id": "0011500001Lqg2UAAR",
                    "qbStatus": null,
                    "qbEditSequence": null,
                    "qbListId": null
                }
            },
            "division": {
                "Id": "a4F1C0000008lT1UAI",
                "name": "Travel Nursing",
                "number": "1"
            },
            "billingCard": {
                "Id": "a4N1C000000LCxYUAW",
                "Billing_Contact__c": "0031500001jqDtuAAE",
                "Billing_Contact_Phone__c": "888-387-8743",
                "Payment_Terms__c": "Net 60",
                "Billing_State__c": "a0o1500000ABoJuAAL",
                "Billing_City__c": "San Diego",
                "Billing_Frequency__c": "Weekly",
                "type": "Billing_Card__c",
                "Billing_Address__c": "12400 High Bluff Drive",
                "Invoice_Email_Address__c": "melissa.reyes@amnhealthcare.com",
                "name": "BC-165",
                "Billing_Address_2__c": "",
                "Division__c": "a4F1C0000008lT1UAI",
                "Billing_Card_Name_text__c": "AMN - Kaiser Permanente - California",
                "Billing_Zip__c": "92130"
            }
        },
        "invoice": {
            "requestRef": {
                "Id": "a2oc000000MHtVVAA1",
                "name": "200058",
                "qbStatus": null,
                "lastModifiedDate": "2018-12-19T15:15:38.000Z"
            },
            "Billing_Contact__c": "0031500001jqDtuAAE",
            "Billing_Contact_Phone__c": "888-387-8743",
            "Date__c": "2018-12-19T15:15:38.000Z",
            "LastReferencedDate": "2018-07-07",
            "Billing_City__c": "San Diego",
            "type": "Invoice__c",
            "CreatedById": "005150000066ZqFAAU",
            "OwnerId": "005150000066ZqFAAU",
            "Billing_Card__c": "a4N1C000000LCxYUAW",
            "Payment_Terms__c": "Net 60",
            "IsDeleted": "false",
            "VMS_Fee__c": "2.0",
            "Billing_State__c": "California",
            "LastViewedDate": null,
            "Billing_Address__c": "12400 High Bluff Drive",
            "SystemModstamp": "2018-12-19T15:15:38.000Z",
            "Discounted_Total_Bill__c": "0.0",
            "Billing_Address_2__c": null,
            "CreatedDate": "2018-12-19T15:15:38.000Z",
            "Division__c": "a4F1C0000008lT1UAI",
            "LastActivityDate": null,
            "LastModifiedById": "005150000066ZqFAAU",
            "VMS_Discount__c": "0.0",
            "Billing_Zip__c": "92130"
        }
    },
    {
        "customer": {
            "requestRef": {
                "Id": "a4O1C000000a3hmUAA",
                "customerName": "Torrance Memorial Medical Center-2",
                "qbStatus": null,
                "qbEditSequence": null,
                "qbListId": null,
                "lastModifiedDate": "2018-06-18T15:45:17.000Z"
            },
            "account": {
                "Id": "0011500001Lqg0aAAB",
                "name": "Torrance Memorial Medical Center",
                "billingCity": "Torrance",
                "billingStreet": "3330 Lomita Blvd",
                "billingStateProvinceId": "a0o1500000ABoJuAAL",
                "billingStateProvinceName": "California",
                "billingZipPostalCode": "90505",
                "msp": "Aya Connect",
                "requestRef": {
                    "Id": "0011500001Lqg0aAAB",
                    "qbStatus": null,
                    "qbEditSequence": null,
                    "qbListId": null
                }
            },
            "division": {
                "Id": "a4F1C0000008lT2UAI",
                "name": "Allied Health Tech",
                "number": "2"
            },
            "billingCard": {
                "Id": "a4N1C000000LDAIUA4",
                "Billing_Contact__c": "0031C00002MNZ31QAH",
                "Billing_Contact_Phone__c": "(310) 325-9110",
                "Payment_Terms__c": "Net 30",
                "Billing_State__c": "",
                "Billing_City__c": "",
                "Billing_Frequency__c": "Weekly",
                "type": "Billing_Card__c",
                "Billing_Address__c": "",
                "Invoice_Email_Address__c": "lzellinger@hcselect.com",
                "name": "BC-323",
                "Billing_Address_2__c": "",
                "Division__c": "a4F1C0000008lT2UAI",
                "Billing_Card_Name_text__c": "HealthcareSelect - Torrance Memorial Medical Center - Allied Tech",
                "Billing_Zip__c": ""
            }
        },
        "invoice": {
            "requestRef": {
                "Id": "a2oc000000MHtVRAA1",
                "name": "200054",
                "qbStatus": null,
                "lastModifiedDate": "2018-12-19T15:15:37.000Z"
            },
            "Billing_Contact__c": "0031C00002MNZ31QAH",
            "Billing_Contact_Phone__c": "(310) 325-9110",
            "Date__c": "2018-12-19T15:15:37.000Z",
            "LastReferencedDate": "2018-05-05",
            "Billing_City__c": null,
            "type": "Invoice__c",
            "CreatedById": "005150000066ZqFAAU",
            "OwnerId": "005150000066ZqFAAU",
            "Billing_Card__c": "a4N1C000000LDAIUA4",
            "Payment_Terms__c": "Net 30",
            "IsDeleted": "false",
            "VMS_Fee__c": null,
            "Billing_State__c": null,
            "LastViewedDate": null,
            "Billing_Address__c": null,
            "SystemModstamp": "2018-12-19T15:15:37.000Z",
            "Discounted_Total_Bill__c": "0.0",
            "Billing_Address_2__c": null,
            "CreatedDate": "2018-12-19T15:15:37.000Z",
            "Division__c": "a4F1C0000008lT2UAI",
            "LastActivityDate": null,
            "LastModifiedById": "005150000066ZqFAAU",
            "VMS_Discount__c": "0.0",
            "Billing_Zip__c": null
        }
    },
    {
        "customer": {
            "requestRef": {
                "Id": "a4O1C000000a3hmUAA",
                "customerName": "Torrance Memorial Medical Center-2",
                "qbStatus": null,
                "qbEditSequence": null,
                "qbListId": null,
                "lastModifiedDate": "2018-06-18T15:45:17.000Z"
            },
            "account": {
                "Id": "0011500001Lqg0aAAB",
                "name": "Torrance Memorial Medical Center",
                "billingCity": "Torrance",
                "billingStreet": "3330 Lomita Blvd",
                "billingStateProvinceId": "a0o1500000ABoJuAAL",
                "billingStateProvinceName": "California",
                "billingZipPostalCode": "90505",
                "msp": "Aya Connect",
                "requestRef": {
                    "Id": "0011500001Lqg0aAAB",
                    "qbStatus": null,
                    "qbEditSequence": null,
                    "qbListId": null
                }
            },
            "division": {
                "Id": "a4F1C0000008lT2UAI",
                "name": "Allied Health Tech",
                "number": "2"
            },
            "billingCard": {
                "Id": "a4N1C000000LDAIUA4",
                "Billing_Contact__c": "0031C00002MNZ31QAH",
                "Billing_Contact_Phone__c": "(310) 325-9110",
                "Payment_Terms__c": "Net 30",
                "Billing_State__c": "",
                "Billing_City__c": "",
                "Billing_Frequency__c": "Weekly",
                "type": "Billing_Card__c",
                "Billing_Address__c": "",
                "Invoice_Email_Address__c": "lzellinger@hcselect.com",
                "name": "BC-323",
                "Billing_Address_2__c": "",
                "Division__c": "a4F1C0000008lT2UAI",
                "Billing_Card_Name_text__c": "HealthcareSelect - Torrance Memorial Medical Center - Allied Tech",
                "Billing_Zip__c": ""
            }
        },
        "invoice": {
            "requestRef": {
                "Id": "a2oc000000MHtVUAA1",
                "name": "200057",
                "qbStatus": null,
                "lastModifiedDate": "2018-12-19T15:15:38.000Z"
            },
            "Billing_Contact__c": "0031C00002MNZ31QAH",
            "Billing_Contact_Phone__c": "(310) 325-9110",
            "Date__c": "2018-12-19T15:15:38.000Z",
            "LastReferencedDate": "2018-06-02",
            "Billing_City__c": null,
            "type": "Invoice__c",
            "CreatedById": "005150000066ZqFAAU",
            "OwnerId": "005150000066ZqFAAU",
            "Billing_Card__c": "a4N1C000000LDAIUA4",
            "Payment_Terms__c": "Net 30",
            "IsDeleted": "false",
            "VMS_Fee__c": null,
            "Billing_State__c": null,
            "LastViewedDate": null,
            "Billing_Address__c": null,
            "SystemModstamp": "2018-12-19T15:15:38.000Z",
            "Discounted_Total_Bill__c": "0.0",
            "Billing_Address_2__c": null,
            "CreatedDate": "2018-12-19T15:15:38.000Z",
            "Division__c": "a4F1C0000008lT2UAI",
            "LastActivityDate": null,
            "LastModifiedById": "005150000066ZqFAAU",
            "VMS_Discount__c": "0.0",
            "Billing_Zip__c": null
        }
    }
]