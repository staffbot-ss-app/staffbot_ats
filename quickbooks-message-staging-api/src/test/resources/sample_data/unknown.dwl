{
    "qbRequestIndex": 1,
    "qbRequestCount": 1,
    "qbRequestStep": "Initial",
    "msp": "Aya Connect",
    "qbRequestData": [
        {
            "facilityBillingCardAssocId": "a4O1C000000a3ltUAA",
            "accountId": "0011C00001uQ8ZdQAK",
            "billingCardId": "a4N1C000000LD6fUAG",
            "divisionId": "a4F1C0000008laTUAQ",
            "divisionName": "MedFi",
            "accountName": "Absolute Total Care",
            "billingCity": "Columbia",
            "billingStreet": "1441 Main St",
            "billingStateProvince": "a0o1500000ABoKTAA1",
            "billingZipPostalCode": "29201",
            "customerName": "Absolute Total Care-MedFi",
            "msp": "Aya Connect",
            "billingCards": {
                "Billing_Contact__c": "0031C00002JRW9XQAX",
                "Billing_Contact_Phone__c": null,
                "Payment_Terms__c": "Net 45",
                "Billing_City__c": "St Louis",
                "Billing_Frequency__c": "Weekly",
                "type": "Billing_Card__c",
                "Billing_Address__c": "7700 Forsyth Blvd",
                "Invoice_Email_Address__c": "rspetz@coordinatedcarehealth.com",
                "Name": "BC-278",
                "Billing_Address_2__c": null,
                "Division__c": "a4F1C0000008laTUAQ",
                "Id": "a4N1C000000LD6fUAG",
                "Billing_Card_Name_text__c": "Centene"
            },
            "QBMessageStatus": "Initialized",
            "customerAddOrUpdate": null
        }
    ]
}