{
    "requestType": "customer",
    "requestRef": {
        "Id": "a4O1C000000a3AiUAI",
        "customerName": "Generic-1",
        "qbStatus": null,
        "qbEditSequence": null,
        "qbListId": null,
        "lastModifiedDate": "2018-06-04T18:17:00.000Z"
    },
    "account": {
        "Id": "0011C00001wFs77QAC",
        "name": "Generic",
        "billingCity": "not listed",
        "billingStreet": "not listed",
        "billingStateProvinceId": "a0o1500000ABoKcAAL",
        "billingStateProvinceName": "Wisconsin",
        "billingZipPostalCode": "54956",
        "msp": "Qualivis",
        "requestRef": {
            "Id": "0011C00001wFs77QAC",
            "qbStatus": null,
            "qbEditSequence": null,
            "qbListId": null
        }
    },
    "division": {
        "Id": "a4F1C0000008lT1UAI",
        "name": "Travel Nursing",
        "number": "1"
    },
    "billingCard": {
        "Id": "a4N1C000000LCsYUAW",
        "Billing_Contact__c": "",
        "Billing_Contact_Phone__c": "",
        "Payment_Terms__c": "Net 30",
        "Billing_State__c": "",
        "Billing_City__c": "",
        "Billing_Frequency__c": "Weekly",
        "type": "Billing_Card__c",
        "Billing_Address__c": "",
        "Invoice_Email_Address__c": "",
        "name": "BC-111",
        "Billing_Address_2__c": "",
        "Division__c": "a4F1C0000008lT1UAI",
        "Billing_Card_Name_text__c": "Generic Test",
        "Billing_Zip__c": "37934"
    }
}