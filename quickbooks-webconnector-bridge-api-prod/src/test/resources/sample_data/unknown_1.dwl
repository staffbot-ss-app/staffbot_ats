<?xml version='1.0'?>
<QBXML>
  <QBXMLMsgsRs>
    <CustomerQueryRs requestID="4" statusCode="0" statusSeverity="Info" statusMessage="Status OK">
      <CustomerRet>
        <ListID>800000E8-1702702433</ListID>
        <TimeCreated>2023-12-15T22:53:53-06:00</TimeCreated>
        <TimeModified>2023-12-15T22:53:53-06:00</TimeModified>
        <EditSequence>1702702433</EditSequence>
        <Name>Aya Connect</Name>
        <FullName>Aya Connect</FullName>
        <IsActive>true</IsActive>
        <Sublevel>0</Sublevel>
        <ShipToAddress>
          <Name>Ship To 1</Name>
          <Addr1>1441 Main St</Addr1>
          <City>Columbia</City>
          <State>29201</State>
          <PostalCode>29201</PostalCode>
          <DefaultShipTo>false</DefaultShipTo>
        </ShipToAddress>
        <TermsRef>
          <ListID>20000-933272658</ListID>
          <FullName>Net 15</FullName>
        </TermsRef>
        <Balance>0.00</Balance>
        <TotalBalance>0.00</TotalBalance>
        <SalesTaxCodeRef>
          <ListID>10000-999022286</ListID>
          <FullName>Tax</FullName>
        </SalesTaxCodeRef>
        <ItemSalesTaxRef>
          <ListID>2E0000-933272656</ListID>
          <FullName>San Tomas</FullName>
        </ItemSalesTaxRef>
        <JobStatus>None</JobStatus>
        <PreferredDeliveryMethod>None</PreferredDeliveryMethod>
      </CustomerRet>
    </CustomerQueryRs>
    <CustomerQueryRs requestID="4" statusCode="0" statusSeverity="Info" statusMessage="Status OK">
      <CustomerRet>
        <ListID>800000E9-1702702433</ListID>
        <TimeCreated>2023-12-15T22:53:53-06:00</TimeCreated>
        <TimeModified>2023-12-15T22:53:53-06:00</TimeModified>
        <EditSequence>1702702433</EditSequence>
        <Name>Absolute Total Care-MedFi</Name>
        <FullName>Aya Connect:Absolute Total Care-MedFi</FullName>
        <IsActive>true</IsActive>
        <ParentRef>
          <ListID>800000E8-1702702433</ListID>
          <FullName>Aya Connect</FullName>
        </ParentRef>
        <Sublevel>1</Sublevel>
        <CompanyName>Absolute Total Care-MedFi</CompanyName>
        <BillAddress>
          <Addr1>1441 Main St</Addr1>
          <City>Columbia</City>
          <State>29201</State>
          <PostalCode>29201</PostalCode>
        </BillAddress>
        <BillAddressBlock>
          <Addr1>1441 Main St</Addr1>
          <Addr2>Columbia, 29201 29201</Addr2>
        </BillAddressBlock>
        <ShipAddress>
          <Addr1>1441 Main St</Addr1>
          <City>Columbia</City>
          <State>29201</State>
          <PostalCode>29201</PostalCode>
        </ShipAddress>
        <ShipAddressBlock>
          <Addr1>1441 Main St</Addr1>
          <Addr2>Columbia, 29201 29201</Addr2>
        </ShipAddressBlock>
        <ShipToAddress>
          <Name>Ship To 1</Name>
          <Addr1>1441 Main St</Addr1>
          <City>Columbia</City>
          <State>29201</State>
          <PostalCode>29201</PostalCode>
          <DefaultShipTo>true</DefaultShipTo>
        </ShipToAddress>
        <Email>rspetz@coordinatedcarehealth.com</Email>
        <AdditionalContactRef>
          <ContactName>Main Email</ContactName>
          <ContactValue>rspetz@coordinatedcarehealth.com</ContactValue>
        </AdditionalContactRef>
        <CustomerTypeRef>
          <ListID>10000-933272658</ListID>
          <FullName>Commercial</FullName>
        </CustomerTypeRef>
        <Balance>0.00</Balance>
        <TotalBalance>0.00</TotalBalance>
        <SalesTaxCodeRef>
          <ListID>10000-999022286</ListID>
          <FullName>Tax</FullName>
        </SalesTaxCodeRef>
        <ItemSalesTaxRef>
          <ListID>2E0000-933272656</ListID>
          <FullName>San Tomas</FullName>
        </ItemSalesTaxRef>
        <JobStatus>None</JobStatus>
        <PreferredDeliveryMethod>None</PreferredDeliveryMethod>
      </CustomerRet>
    </CustomerQueryRs>
  </QBXMLMsgsRs>
</QBXML>