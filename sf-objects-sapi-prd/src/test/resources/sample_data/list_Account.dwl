[
  {
    "Billing_Zip_Postal_Code__c": "79938",
    "VMS1__c": "Qualivis",
    "Billing_Street__c": "3280 Joe Battle Blvd",
    "Id": "0011500001LqgBEAAZ",
    "Billing_City__c": "El Paso",
    "Billing_State_Province__c": "a0o1500000ABoKWAA1",
    "type": "Account",
    "Name": "Hospitals of Providence East Campus"
  },
  {
    "Billing_Zip_Postal_Code__c": "85016",
    "VMS1__c": "Medefis",
    "Billing_Street__c": "1919 East Thomas Road",
    "Id": "0011500001ejCXhAAM",
    "Billing_City__c": "Phoenix",
    "Billing_State_Province__c": "a0o1500000ABoJsAAL",
    "type": "Account",
    "Name": "Phoenix Childrens Hospital"
  },
  {
    "Billing_Zip_Postal_Code__c": "46601",
    "VMS1__c": null,
    "Billing_Street__c": "615 N Michigan St",
    "Id": "0011500001eozdWAAQ",
    "Billing_City__c": "South Bend",
    "Billing_State_Province__c": "a0o1500000ABoK3AAL",
    "type": "Account",
    "Name": "Memorial Hospital of South Bend"
  },
  {
    "Billing_Zip_Postal_Code__c": "46805",
    "VMS1__c": "FocusOne Solutions",
    "Billing_Street__c": "2200 Randallia Drive",
    "Id": "0011500001gC5fbAAC",
    "Billing_City__c": "Fort Wayne",
    "Billing_State_Province__c": "a0o1500000ABoK3AAL",
    "type": "Account",
    "Name": "Parkview Hospital Randallia"
  },
  {
    "Billing_Zip_Postal_Code__c": "68502",
    "VMS1__c": "FocusOne Solutions",
    "Billing_Street__c": "2300 S 16th St.",
    "Id": "0011500001hDXhYAAW",
    "Billing_City__c": "Lincoln",
    "Billing_State_Province__c": "a0o1500000ABoKGAA1",
    "type": "Account",
    "Name": "Bryan West Campus"
  }
]